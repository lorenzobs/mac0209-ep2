import sys
import matplotlib.pyplot as pyplot
import math

# DADOS
espess = 0.8 #° (fio)
massa = 8.37 #g (esfera)
comp = 29.05 #cm (fio)
g = 980 # cm/s²

def maxAbs(x):
	'''
		Retorna o maior valor absoluto do vetor x
	'''
	max = 0

	for xi in x:
		if abs(xi) > max:
			max = abs(xi)
		
	return max

def convAngToCm(angulo):
	''' 
	Recebe um vetor de angulos e converte para centímetros, utilizando a fórmula do arco: x = teta*pi*raio/180
	'''
	posicao = []
	for i in range(0,len(angulo)):
		a = angulo[i]
		pp = []
		for j in range(0,len(a)):
			pp.append((a[j]*math.pi*comp)/180)
		posicao.append(pp)
	return posicao

def coletaDados(arquivo):
	'''
	Função que recebe o nome do arquivo contendo os dados do experimentos e armazena essas informações em tempo e graus.
	Obs: posição = 0° quer dizer que o fio está na vertical, posição < 0 quer dizer que o fio está à direita
	e posição > 0 o fio está à esquerda.
	Como o dado de espessura do fio foi fornecido em graus, todas as medidas seguintes, incluindo as velocidades,
	utilizarão o grau como unidade de medida.
	'''
	tempo = []
	angulo = []
	with open(arquivo,"r") as txt:
		 qtd = txt.readline()
		 while (qtd != ""):	 
		 	tt = []
		 	aa = []
		 	for i in range(0,int(qtd)):
		 		linha = txt.readline()
		 		linha = linha.replace("\n","")
		 		x = linha.split(" ")
		 		t = float(x[0])
		 		# Medida em graus é em relação ao eixo vertical.
		 		a = float(x[1])+ espess/2 -270
		 		tt.append(t)
		 		aa.append(a)	 	
		 	tempo.append(tt)
		 	angulo.append(aa)
		 	qtd = txt.readline()

	return [tempo,angulo]


def posicaoGraus(tempo,angulo):
	'''
	Função que plota a posição do pêndulo em graus x tempo
	'''
	for i in range(0,len(tempo)):
		label = "FV" + str(i+1)
		# Gráfico sobreposto
		plot(tempo[i],angulo[i],label,0,0,"","Sobreposição dos experimentos","Tempo(s)","Posição(°)",0)

		#Gráfico individual
		plot(tempo[i],angulo[i],"",0,0,"","Posição(°) " + label,"Tempo(s)","Posição(°)",i+1)

	pyplot.show()
	return

def posicaoCm(tempo,posicao):
	'''
	Função que plota a posição do pêndulo em centímetros x tempo.
	'''

	for i in range(0,len(tempo)):
		label = "FV" + str(i+1)
		# Gráfico sobreposto
		plot(tempo[i],posicao[i],label,0,0,"","Sobreposição dos experimentos","Tempo(s)","Posição(cm)",0)

		# Gráfico individual
		plot(tempo[i],posicao[i],"",0,0,"","Posição(cm) " + label,"Tempo(s)","Posição(cm)",i+1)

	
	pyplot.show()
	return

def velGraus(tempo,angulo):
	'''
	Função que plota a velocidade do pêndulo em graus x tempo e calcula a velocidade média e média das velocidades.
	'''

	vel_media_total = 0
	media_vel_total = 0

	for i in range(0,len(tempo)):
		label = "FV" + str(i+1)
		t = tempo[i]
		a = angulo[i]
		tam = len(tempo[i])

		# Cálculo velocidade média
		vel_media = ( abs(a[tam-1] - a[0]) )/( t[tam-1] - t[0] )
		print("Velocidade média do",label,"é:",vel_media,"°/s")

		# Cálculo media das velocidades e plot da velocidade
		media_vel = 0
		v = []
		dt = []
		t_acum = 0
		for j in range(0,tam-1):
			media_vel = media_vel + abs(a[j+1]-a[j])/(t[j+1]-t[j])
			t_acum = t_acum + t[j+1]-t[j]
			dt.append(t_acum)
			v.append(abs((a[j+1]-a[j]))/(t[j+1]-t[j]))

		# Gráfico sobreposto
		plot(dt,v,label,0,0,"","Sobreposição dos experimentos","Tempo(s)","Velocidade(°/s)",0)

		#Gŕafico individual
		plot(dt,v,"",0,0,"","Velocidade(°/s) " + label,"Tempo(s)","Velocidade(°/s)",i+1)

		media_vel = media_vel/(tam-1)
		print("Média das velocidades do",label,"é:",media_vel,"°/s")

		vel_media_total += vel_media
		media_vel_total += media_vel


	vel_media_total = vel_media_total / len(tempo)
	media_vel_total = media_vel_total / len(tempo)
	print("Média das velocidades médias, considerando os 5 experimentos:",vel_media_total,"°/s")
	print("Média da média das velocidades, considerando os 5 experimentos",media_vel_total,"°/s")

	pyplot.show()
	return

def velCm(tempo,posicao):
	'''
	Função que plota a velocidade do pêndulo em centímetros x tempo e calcula a velocidade média e média das velocidades.
	'''
	vel_media_total = 0
	media_vel_total = 0

	for i in range(0,len(tempo)):
		label = "FV" + str(i+1)
		t = tempo[i]
		p = posicao[i]
		tam = len(tempo[i])

		# Cálculo velocidade média
		vel_media = ( abs(p[tam-1] - p[0]) )/( t[tam-1] - t[0] )
		print("Velocidade média do",label,"é:",vel_media,"cm/s")

		# Cálculo media das velocidades
		media_vel = 0
		v = []
		dt = []
		t_acum = 0
		for j in range(0,tam-1):
			media_vel = media_vel + abs(p[j+1]-p[j])/(t[j+1]-t[j])
			t_acum = t_acum + t[j+1]-t[j]
			dt.append(t_acum)
			v.append((abs(p[j+1]-p[j]))/(t[j+1]-t[j]))

		# Gráfico sobreposto
		plot(dt,v,label,0,0,"","Sobreposição dos experimentos","Tempo(s)","Velocidade(cm/s)",0)

		#Gŕafico individual
		plot(dt,v,"",0,0,"","Velocidade(cm/s) " + label,"Tempo(s)","Velocidade(cm/s)",i+1)

		media_vel = media_vel/(tam-1)
		print("Média das velocidades do",label,"é:",media_vel,"cm/s")

		vel_media_total += vel_media
		media_vel_total += media_vel


	vel_media_total = vel_media_total / len(tempo)
	media_vel_total = media_vel_total / len(tempo)
	print("Média das velocidades média, considerando os 5 experimentos:",vel_media_total,"cm/s")
	print("Média da média das velocidades, considerando os 5 experimentos",media_vel_total,"cm/s")

	pyplot.show()
	return

def acelGraus(tempo,angulo):
	'''
	Função que plota a aceleração do pêndulo em graus x tempo.
	'''

	for i in range(0,len(tempo)):
		label = "FV" + str(i+1)
		t = tempo[i]
		a = angulo[i]
		tam = len(tempo[i])

		v = []
		dt = []
		t_acum = 0
		for j in range(0,tam-1):
			t_acum = t_acum + t[j+1]-t[j]
			dt.append(t_acum)
			v.append(((a[j+1]-a[j]))/(t[j+1]-t[j]))

		t_acum = 0
		dt2 = []
		ac = []
		for j in range(0,len(v)-1):
			t_acum = t_acum + dt[j+1]-dt[j]
			dt2.append(t_acum)
			ac.append((v[j+1]-v[j])/(dt[j+1]-dt[j]))

		# Gráfico sobreposto
		plot(dt2,ac,label,0,0,"","Sobreposição dos experimentos","Tempo(s)","Aceleração(°/s²)",0)

		#Gŕafico individual
		plot(dt2,ac,"",0,0,"","Aceleração(°/s²) " + label,"Tempo(s)","Aceleração(°/s²)",i+1)

	pyplot.show()
	return	

def acelCm(tempo,posicao):
	'''
	Função que plota a aceleração do pêndulo em centímetros x tempo.
	'''

	for i in range(0,len(tempo)):
		label = "FV" + str(i+1)
		t = tempo[i]
		p = posicao[i]
		tam = len(tempo[i])

		v = []
		dt = []
		t_acum = 0
		for j in range(0,tam-1):
			t_acum = t_acum + t[j+1]-t[j]
			dt.append(t_acum)
			v.append((abs(p[j+1]-p[j]))/(t[j+1]-t[j]))

		t_acum = 0
		dt2 = []
		ac = []
		for j in range(0,len(v)-1):
			t_acum = t_acum + dt[j+1]-dt[j]
			dt2.append(t_acum)
			ac.append((abs(v[j+1]-v[j]))/(dt[j+1]-dt[j]))

		# Gráfico sobreposto
		plot(dt2,ac,label,0,0,"","Sobreposição dos experimentos","Tempo(s)","Aceleração(cm/s²)",0)

		#Gŕafico individual
		plot(dt2,ac,"",0,0,"","Aceleração(cm/s²) " + label,"Tempo(s)","Aceleração(cm/s²)",i+1)

	pyplot.show()
	return	

def periodoMedio(tempo,angulo):
	'''
	Calcula o período médio de cada experimento e calcula o perído médio de todos os experimentos
	'''
	periodoMedio = 0
	for i in range(0,len(angulo)):
		t = tempo[i]
		a = angulo[i]
		a0 = a[0]
		t0 = 0
		periodo = 0
		vezes = 0
		for j in range(1,len(a)):
			if (a[j] - a0 <= 0.1):
				vezes += 1
				periodo = periodo + t[j] - t0
				t0 = t[j]
		periodo = periodo / vezes
		periodoMedio = periodoMedio + periodo
		print("O período médio do experimento FV",(i+1),"é:",periodo,"s")
	periodoMedio = periodoMedio / len(angulo)
	print("O período médio de todos os experimentos é:",periodoMedio,"s")
	return

def altura(angulo):
	'''
	Função que recebe um ângulo e calcula a altura que o pêndulo está em relação a posição de equilíbrio
	'''
	angulo = math.pi * angulo/180
	cos = math.cos(angulo)
	return comp * (1 - cos)

def potencial(altura):
	'''
	Função que recebe uma altura e calcula a energia potencial do pêndulo nessa altura
	'''
	return massa * g * altura

def cinetica(velocidade):
	'''
	Função que recebe uma velocidade e calcula a energia cinética do pêndulo com essa velocidade
	'''
	return massa * velocidade * velocidade / 2

def novaVel(altura,total):
	'''
	Função que recebe uma altura e a energia total do sistema e calcula a velocidade do pendulo.
	'''
	Ep = potencial(altura)
	Ec = total - Ep
	v2 = 2*Ec/massa
	return math.sqrt(v2)

def novoAng(a0,t): 
	'''
	Função que usa a fórmula A(t) = A0 * cos(W*t), sendo A0 a amplitude máxima do pêndulo e W=g/L para calcular
	o ângulo do pendulo com o eixo vertical
	'''
	w = math.sqrt(g/comp)	
	return a0 * math.cos(w*t)

def novaAcel(a):
	'''
	Função que calcula a próxima aceleração angular do pêndulo
	'''
	return (-g/comp)*a 

def alteraAmplitude(a0,t,T,bol):
	'''
	Função que considera o atrito no pêndulo
	'''
	if bol == False:
		return a0
	atrito = 0.009 # coeficiente de atrito
	I = (massa/1000)*(comp/10)*(comp/10) # unidades do sistema internacional
	return (a0 * math.pow(math.e,-atrito*t/(2*I)))

def atualizaPasso(t,dt,v,a,h,a0,et):
	'''
	Função que atualiza o movimento do pêndulo, chamando as funções auxiliares
	'''
	t = t + dt
	a = novoAng(a0,t)
	acc = novaAcel(a)
	h = altura(abs(a))
	v = novaVel(h,et)
	return [t,a,acc,h,v]	

def experimento(param):
	'''
	Função que chama todas as análises necessárias do experimento
	'''
	posicao = convAngToCm(param[1])
	posicaoGraus(param[0],param[1])
	posicaoCm(param[0],posicao)
	velGraus(param[0],param[1])
	velCm(param[0],posicao)
	acelGraus(param[0],param[1])
	acelCm(param[0],posicao)
	periodoMedio(param[0],param[1])
	return 

def modelo(boolatrito):
	'''
	Função que modela o comportamento do pêndulo
	'''
	periodo = 2 * math.pi * math.sqrt(comp/g)
	print("O período do modelo é:",periodo,"s")
	a0 =  -1*(9.5 + espess/2) # = 9.9° , ângulo inicial.
	amp = a0
	a = a0	
	v = 0 # partindo do repouso
	t = 0
	tf = 2*periodo # duas oscilações completas
	dt = 0.05
	h = altura(a)
	acc = novaAcel(a0)
	vel = []
	tt = []
	aa = []
	acel = []
	energia_total = cinetica(v) + potencial(h)
	while (t <= tf):		
		vel.append(v)
		tt.append(t)
		aa.append(a)
		acel.append(acc)
		resp = atualizaPasso(t,dt,v,a,h,a0,energia_total)
		t = resp[0]
		a = resp[1]
		acc = resp[2]
		h = resp[3]
		v = resp[4]
		a0 = alteraAmplitude(amp,t,periodo,boolatrito)
		energia_total = potencial(altura(abs(a0)))


	posCm = convAngToCm([aa])[0] # converte posições em grau para posições em cm

	# Calculando velocidade média e média das velocidades
	vel_media = (posCm[len(posCm)-1] - posCm[0]) / abs(tt[len(tt)-1]-tt[0])
	media_vel = 0
	for i in vel:
		media_vel += i
	media_vel = media_vel / len(vel)
	print("Velocidade média (modelo): ",vel_media,"cm/s")
	print("Média das velocidades (modelo): ", media_vel, "cm/s")

	# Posição em graus
	plot(tt,aa,"",0,0,"","Ângulação x Tempo (modelo)","Tempo(s)","Posição(°)",-1)

	# Posição em cm
	plot(tt,posCm,"",0,0,"","Posição x Tempo (modelo)","Tempo(s)","Posição(cm)",-1)

	# Velocidade em cm/s
	plot(tt,vel,"",0,0,"","Velocidade x Tempo (modelo)","Tempo(s)","Velocidade(cm/s)",-1)

	# Aceleração angular (°/s²)
	plot(tt,acel,"",0,0,"","Aceleração angular x Tempo (modelo)","Tempo(s)","Aceleração(°/s²)",-1)

	return [tt,aa,posCm,vel,acel]

def compara(paramModelo,paramExperi):
	'''
	Função que compara as informações de posição, velocidade e aceleração entre o modelo e algum experimento
	'''
	# Refazendo os cálculos para o experimento 1
	experimento = int(input("Qual experimento deseja comparar com o modelo(1-5): ")) - 1
	tempo = paramExperi[0][experimento]
	angulo = paramExperi[1][experimento]
	espaco = convAngToCm(paramExperi[1])[experimento]
	velcm = []
	velang = []
	acc = []
	tvel = []
	for i in range(0,len(tempo)-1):
		v = (abs(espaco[i+1]-espaco[i]))/(tempo[i+1]-tempo[i])
		va = (angulo[i+1]-angulo[i])/(tempo[i+1]-tempo[i])
		if (len(tvel) == 0):
			ultimo = 0
		else:
			ultimo = tvel[len(tvel)-1]
		tvel.append(ultimo+tempo[i+1]-tempo[i])
		velcm.append(v)
		velang.append(va)
	tacc = []
	for i in range(0,len(velcm)-1):
		a = (velang[i+1]-velang[i])/(tvel[i+1]-tvel[i])
		if (len(tacc) == 0):
			ultimo = 0
		else:
			ultimo = tacc[len(tacc)-1]
		tacc.append(ultimo+tvel[i+1]-tvel[i])
		acc.append(a)

	# Plotando os gŕaficos
	pyplot.close('all')

	plot(tempo,angulo,"Experimento",paramModelo[0],paramModelo[1],"Modelo","Modelo x experimento - Posição(°)","Tempo(s)","Posição(°)",-1)

	plot(tempo,espaco,"Experimento",paramModelo[0],paramModelo[2],"Modelo","Modelo x experimento - Posição(cm)","Tempo(s)","Posição(cm)",-1)

	plot(tvel,velcm,"Experimento",paramModelo[0],paramModelo[3],"Modelo","Modelo x experimento - Velocidade(cm/s)","Tempo(s)","Velocidade(cm/s)",-1)

	plot(tacc,acc,"Experimento",paramModelo[0],paramModelo[4],"Modelo","Modelo x experimento - Aceleração(°/s²)","Tempo(s)","Aceleração(°/s²)",-1)


	pyplot.show()
	return

def plot(v1,v2,legenda1,v3,v4,legenda2,title,xlabel,ylabel,i):
	if i != -1:
		pyplot.figure(i)
	else:
		pyplot.figure()
	pyplot.plot(v1,v2,label=legenda1)
	if (v3 != 0 and v4 != 0):
		pyplot.plot(v3,v4,label=legenda2)	

	if (legenda1 == ""):
		pyplot.plot(v1[-1],v2[-1],"ro")
		pyplot.annotate("(%.2f,%.2f)" %(v1[-1],v2[-1]), xy=(v1[-1], v2[-1]), xytext=(v1[-1] - maxAbs(v1)*0.2, v2[-1]))

	pyplot.title(title)
	pyplot.xlabel(xlabel)
	pyplot.ylabel(ylabel)
	if (legenda1 != "" or legenda2 != ""):
		pyplot.legend()
	return

def testaConvAngToCm():
	''' 
	Testa a função convAngToCm
	'''
	angulo = [[360]]
	pos = convAngToCm(angulo)
	if (pos[0][0] - 2*math.pi*comp > 0.000001):
		print("Erro com angulo 360°",pos[0][0],"!=",2*math.pi*comp)
	angulo = [[90]]
	pos = convAngToCm(angulo)
	if (pos[0][0] - math.pi*comp/2 > 0.000001):
		print("Erro com angulo 90°",pos[0][0],"!=",2*math.pi*comp)
	angulo = [[5]]
	pos = convAngToCm(angulo)
	if (pos[0][0] - math.pi*comp/36 > 0.000001):
		print("Erro com angulo 5°",pos[0][0],"!=",2*math.pi*comp)
	return

def testaNovaVel():
	''' 
	Testa a função novaVel
	'''
	if (novaVel(10,massa*g*10) != 0):
		print("Problema com novaVel(caso1)")
	if (novaVel(10,massa*g*20) != 140):
		print("Problema com novaVel(caso2)")
	if (novaVel(0,0) != 0):
		print("Problema com novaVel(caso3)")
	return
def testaAltura():
	''' 
	Testa a função altura
	'''
	if (altura(90) - comp > 0.00001):
		print("Problema com altura(90)")
	if (altura(0) - 0 > 0.00001):
		print("Problema com altura(0)")
	if (altura(180)-2*comp > 0.00001):
		print("Problema com altura(180)")
	if (altura(45)- comp/2 > 0.00001):
		print("Problema com altura(45)")
	return

def testes():
	''' 
	Chama todos os testes
	'''
	testaConvAngToCm()
	testaNovaVel()
	testaAltura()
	print("Fim dos testes")
	return

def main():
	if len(sys.argv) != 2:
		print(" Modo de execução:\n\tpython %s <nomeArq>\n Onde <nomeArq> é o nome de um arquivo.txt neste mesmo diretório que contem os dados de experimentos de movimento pendular simples a serem analisados." %sys.argv[0])
		return
	arquivo = sys.argv[1]	
	param = coletaDados(arquivo)
	while (True):
		modo = int(input("O que deseja ver?\n1.Experimentos\n2.Modelo\n3.Comparação\n4.Testes\n5.Sair\n"))
		if modo == 1:
			experimento(param)		
		elif modo == 2:		
			atrito = input("Deseja modelar com atrito? ")
			if atrito == "S" or atrito == "Sim" or atrito == "sim" or atrito == "s":
				atrito = True
			else:
				atrito = False
			paramModelo = modelo(atrito)
		elif modo == 3:
			atrito = input("Deseja modelar com atrito? ")
			if atrito == "S" or atrito == "Sim" or atrito == "sim" or atrito == "s":
				atrito = True
			else:
				atrito = False
			paramModelo = modelo(atrito)
			compara(paramModelo,param)
		elif modo == 4:
			testes()
		else:
			quit()
		pyplot.show()

main()