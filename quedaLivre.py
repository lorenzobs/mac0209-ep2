import sys
import matplotlib.pyplot as pyplot
import numpy as np

## Funções Auxiliares Globais

def leDados(nomeArq):
	'''
	  Função responsável por ler os dados de um arquivo .txt
	contendo os dados de um número qualquer de experimentos.

	O formato esperado para cada experimento é 
	<n>
	<t1> <y1>
	<t2> <y2>
	  :   :
	  :   :
	<tn> <yn>
	  Onde <n> é o número de quadros passados, cada um contendo
	o instante <ti> (em segundos) de quando foi tirada a "foto"
	e a que distância o ponto de referencia se encontra da posi-
	ção inicial (em cm).
 
	  Retorna uma tupla de dois vetores: Um contendo k vetores
	com os 'tempos' de cada instante de cada um dos k experi-
	mentos lidos e outro com k vetores com as posições do objeto
	para cada instante do tempo.
	'''

	tempos = []
	ys = []

	with open(nomeArq,"r") as txt:
		nPontos = txt.readline()
		while (nPontos != ""):	 
			tempo = []
			y = []
			for i in range(0,int(nPontos)):
				linha = txt.readline()
				linha = linha.replace("\n","")
				linha = linha.split(" ")

				tempo.append(float(linha[0]))
				y.append(-float(linha[1]))
			tempos.append(tempo)
			ys.append(y)
			nPontos = txt.readline()

	return (tempos,ys)

def maxAbs(x):
	'''
		Retorna o maior valor absoluto do vetor x
	'''
	max = 0

	for xi in x:
		if abs(xi) > max:
			max = abs(xi)
		
	return max

def plotUnicoFinal(x,y,lbx,lby,titulo,indice=-1):
	'''
	  Plota as grandezas x e y em um gráfico, dando
	ênfase especial ao ponto final.
	'''
	if indice != -1:
		pyplot.figure(indice)
	else:
		pyplot.figure()

	pyplot.plot(x,y)
	pyplot.plot(x[-1],y[-1],"ro")

	pyplot.annotate("(%.3f,%.3f)" %(x[-1],y[-1]), xy=(x[-1], y[-1]), xytext=(x[-1] - maxAbs(x)*0.26, y[-1]))

	pyplot.title(titulo)
	pyplot.xlabel(lbx)
	pyplot.ylabel(lby)
	
def plotMultiplo(xs,ys,lbx,lby,titulo,indice,legendas):
	'''
	  Plota de maneira sobreposta gráficos das grandezas
	xs e ys.
	'''

	if indice != -1:
		pyplot.figure(indice)
	else:
		pyplot.figure()

	for i in range(len(xs)):
		pyplot.plot(xs[i],ys[i],label=legendas[i])

	pyplot.title(titulo)
	pyplot.xlabel(lbx)
	pyplot.ylabel(lby)
	
	pyplot.legend()

## Análise dos Experimentos

# No caso do arquivo dadosQuedaLivre.txt, analisamos os experimentos A1, A2, A4, A5 e A6, respectivamente.

def analisaExperimentos(dados):
	'''
	  Função responsável por organizar os dados dos exeperi-
	mentos passados em <dados>, retornando um dicionário com os
	vetores "tempos", "ys", "velocidades", "aceleracoes" - cada
	um com um vetor para as respectivas grandezas em cada um 
	dos experimentos dados. 
	'''

	tempos, ys = dados
	vels = []
	accs = []

	for i in range(len(tempos)):
		tempo = tempos[i]
		y = ys[i]
		vel = []
		acc = []

		for j in range(1,len(tempo)):
			dt = tempo[j] - tempo[j-1]

			vel.append((y[j] - y[j-1])/dt)

			if(j >= 2):
				acc.append((vel[j-1]-vel[j-2])/dt)
			else:
				acc.append(vel[j-1]/dt)

		vels.append(vel)
		accs.append(acc)

	dadosExperimentais = {"tempos": tempos, "ys" : ys, "velocidades": vels, "aceleracoes": accs}

	return dadosExperimentais

def mostraExperimentos(dados):
	'''
	  Função responsável por mostrar ao usuário os dados numéri-
	cos interpretados de cada experimento, tanto na forma de 
	gráficos, quanto pela saída de texto.
	'''

	nomeExperimentos = []
	temposMenos1 = []
	velsMedias = []

	for i in range(len(dados["tempos"])):
		tempo = dados["tempos"][i]
		y = dados["ys"][i]

		velsMedias.append(y[-1]/(tempo[-1]-tempo[0]))

		nomeExperimentos.append("Experimento %d" %(i+1))

		plotUnicoFinal(tempo,y,"Tempo (s)","Altura relativa à inicial (cm)","Experimento %d - Altura x Tempo" %(i+1),i)

	plotMultiplo(dados["tempos"],dados["ys"],"Tempo (s)","Altura relativa à inicial (cm)","Sobreposição - Altura x Tempo",len(nomeExperimentos),nomeExperimentos)

	pyplot.show()

	for i in range(len(dados["tempos"])):
		tempo = dados["tempos"][i][1:]
		temposMenos1.append(tempo)
		vel = dados["velocidades"][i]

		print("Velocidade média do experimento %d: %.3f cm/s" %(i+1,velsMedias[i]))

		nomeExperimentos.append("Experimento %d" %(i+1))

		plotUnicoFinal(tempo,vel,"Tempo (s)","Velocidade Média(cm/s)","Experimento %d - Velocidade Média x Tempo" %(i+1),i)

	mediaVelsMedias = 0

	for media in velsMedias:
		mediaVelsMedias += (media/len(velsMedias))

	print("Média das velocidades médias: %.3f " % mediaVelsMedias)

	plotMultiplo(temposMenos1,dados["velocidades"],"Tempo (s)","Velocidade Média(cm/s)","Sobreposição - Velocidade Média x Tempo",len(nomeExperimentos),nomeExperimentos)

	pyplot.show()

	for i in range(len(temposMenos1)):
		tempo = temposMenos1[i]
		acc = dados["aceleracoes"][i]

		plotUnicoFinal(tempo,acc,"Tempo (s)","Aceleração Média(cm/s²)","Experimento %d - Aceleração Média x Tempo" %(i+1),i)

	plotMultiplo(temposMenos1,dados["aceleracoes"],"Tempo (s)","Aceleracao Média(cm/s)","Sobreposição - Acelereacao Média x Tempo",len(nomeExperimentos),nomeExperimentos)

	pyplot.show()

## Simulação / Modelagem 

DELTAT = 0.01 # s
G = -980.0 # cm/s² 
CoefDampening = 0.3

# Vetor de Estados: 
#  [aceleracao,velocidade,y,t]

def atualizaPasso(estado,dt,atrito):
	'''
		Responsável por uma iteração na modelagem por Euler.
	'''
	if(atrito):
		if(estado[1]) > 0:
			deltaAcc = G -estado[0] -CoefDampening*(estado[1]**2)*dt
		else: 
			deltaAcc = G -estado[0] +CoefDampening*(estado[1]**2)*dt
	else:
		deltaAcc = G - estado[0]
	deltaVel = (estado[0]+deltaAcc)*dt
	deltaY = (estado[1]+deltaVel)*dt

	return np.array([deltaAcc,deltaVel,deltaY,dt])

def modela(atrito,t0,tf):
	'''
		Modela o movimento de queda livre a partir do 
	instante t0 da queda até o instante tf, considerando a altura inicial igual a 0, e considerando o atrito da resistência do ar se atrito == true
	'''

	estado = np.array([G,t0*G,0,t0])

	accs = [estado[0]]
	vels = [estado[1]]
	ys = [estado[2]]
	tempos = [t0]

	dt = DELTAT

	while(estado[3] <= tf):
		estado += atualizaPasso(estado,dt,atrito)
		accs.append(estado[0])
		vels.append(estado[1])
		ys.append(estado[2])
		tempos.append(estado[3])

	return {"tempo" : tempos, "y" : ys, "velocidade": vels, "aceleracao": accs}

def mostraSimulacao(dados):

	plotUnicoFinal(dados["tempo"],dados["y"],"Tempo (s)", "Altura relativa à inicial (cm)","Altura x Tempo (Modelo)")

	velMedia = (dados["y"][-1])/(dados["tempo"][-1]-dados["tempo"][0])

	print("Velocidade média (modelo): %.3f" %velMedia)

	plotUnicoFinal(dados["tempo"],dados["velocidade"],"Tempo (s)","Velocidade Média(cm/s)","Velocidade Média x Tempo (modelo)")

	plotUnicoFinal(dados["tempo"],dados["aceleracao"],"Tempo (s)","Aceleração Média(cm/s²)","Aceleração Média x Tempo (modelo)")

	pyplot.show()

## Comparar Exeperimento - vs Modelo

def compara(dadosExperimentos,dadosModelo):

	velMedia = (dadosModelo["y"][-1])/(dadosModelo["tempo"][-1]-dadosModelo["tempo"][0])

	print("Velocidade média do modelo: %.3f" %velMedia)

	idExperimento = int(input("Qual experimento deseja comparar com o modelo (%d-%d)? " %(min(1,len(dadosExperimentos["tempos"])), len(dadosExperimentos["tempos"])) )) -1

	tempo = dadosExperimentos["tempos"][idExperimento]
	y = dadosExperimentos["ys"][idExperimento]
	vel = dadosExperimentos["velocidades"][idExperimento]
	acc = dadosExperimentos["aceleracoes"][idExperimento]

	plotMultiplo([tempo,dadosModelo["tempo"]],[y,dadosModelo["y"]],"Tempo (s)", "Altura relativa à inicial (cm)","Sobreposição Altura x Tempo",-1,["Experimento %d" %(idExperimento+1), "Modelo"])

	plotMultiplo([tempo[1:],dadosModelo["tempo"]],[vel,dadosModelo["velocidade"]],"Tempo (s)", "Velocidade Média(cm/s)","Sobreposição Velocidade Média x Tempo",-1,["Experimento %d" %(idExperimento+1), "Modelo"])

	plotMultiplo([tempo[1:],dadosModelo["tempo"]],[acc,dadosModelo["aceleracao"]],"Tempo (s)", "Aceleração Média(cm/s²)","Sobreposição Aceleração Média x Tempo",-1,["Experimento %d" %(idExperimento+1), "Modelo"])

	pyplot.show()

## Testes

def criaDadosTeste(idTeste):
	'''
		Cria os dados para casos de teste;
	'''
	tempos = [0.0]
	ys = []
	if idTeste == 1:
		ys.append(42)
		for _ in range(20):
			tempos.append(tempos[-1]+0.1)
			ys.append(42)
	elif idTeste == 2:
		ys.append(0)
		vel = -10
		for _ in range(20):
			tempos.append(tempos[-1]+0.1)
			ys.append(vel*tempos[-1])
	elif idTeste == 3:
		ys.append(0)
		for _ in range(200):
			tempos.append(tempos[-1]+0.01)
			ys.append(G*tempos[-1]**2/2)
	elif idTeste == 4:
		ys.append(0)
		acc = G
		vel = 0
		for _ in range(200):
			tempos.append(tempos[-1]+0.01)
			acc = G + CoefDampening*(vel**2)*0.01
			vel+=acc*0.01
			ys.append(ys[-1] + vel*0.01)
	return ([tempos],[ys])

def testes():
	'''
		Testa os métodos de análise de dados de experi-
	mentos, de criação de gráficos, de modelagem e de
	comparação de resultados de diferentes experimen-
	tos;  
	'''

	basicos = input("Deseja realizar os testes mais básicos? ")
	if basicos == "S" or basicos == "Sim" or basicos == "sim" or basicos == "s":
		print("Teste 1 - Objeto Imóvel")

		dados1 = criaDadosTeste(1)
		dados1 = analisaExperimentos(dados1)
		mostraExperimentos(dados1)

		print("Teste 2 - Objeto com Velocidade Constate")

		dados2 = criaDadosTeste(2)
		dados2 = analisaExperimentos(dados2)
		mostraExperimentos(dados2)

	print("Teste 3 - Objeto em queda livre sem atrito")

	dados3 = criaDadosTeste(3)
	dados3 = analisaExperimentos(dados3)

	compara(dados3,modela(False,0.0,2.0))

	print("Teste 4 - Objeto em queda livre com resistência do ar")

	dados4 = criaDadosTeste(4)
	dados4 = analisaExperimentos(dados4)

	compara(dados4,modela(True,0.0,2.0))

## Main

def main():
	if len(sys.argv) != 2:
		print(" Modo de execução:\n\tpython %s <nomeArq>\n Onde <nomeArq> é o nome de um arquivo.txt neste mesmo diretório que contem os dados de experimentos de queda livre a serem analisados." %sys.argv[0])
		return

	nomeArq = sys.argv[1]
	dados = leDados(nomeArq)
	dadosExperimentos = -1

	while (True):
		modo = int(input("O que deseja ver?\n1.Experimentos\n2.Modelo\n3.Comparação\n4.Testes\n5.Sair\n"))
		if modo == 1:
			if(dadosExperimentos == -1):
				dadosExperimentos = analisaExperimentos(dados)
			mostraExperimentos(dadosExperimentos)
		elif modo == 2:
			atrito = input("Deseja modelar com atrito? ")
			if atrito == "S" or atrito == "Sim" or atrito == "sim" or atrito == "s":
				atrito = True
			else:
				atrito = False

			mostraSimulacao(modela(atrito,0.0,.12))
		elif modo == 3:

			if dadosExperimentos == -1:
				dadosExperimentos = analisaExperimentos(dados)

			atrito = input("Deseja modelar com atrito? ")
			if atrito == "S" or atrito == "Sim" or atrito == "sim" or atrito == "s":
				atrito = True
			else:
				atrito = False
			
			compara(dadosExperimentos,modela(atrito,0.0,0.12))
		elif modo == 4:
			testes()

		else:
			quit()
main()