import sys
import matplotlib.pyplot as pyplot
import math
import numpy

# Experimentos utilizados : AM4,AM5,AM6,AM9,AM10
# Dados:
g = 980 # cm/s²
massa = 380.7 # g
altura = 7.6 # cm
baseMaior = 2.79 # cm
baseMenor = 3.64 # cm

labels = ["AM4","AM5","AM6","AM9","AM10"]

CoefsAtrito = [0.31,0.185,0.255,0.285,0.28]

def maxAbs(x):
	'''
		Retorna o maior valor absoluto do vetor x
	'''
	max = 0

	for xi in x:
		if abs(xi) > max:
			max = abs(xi)
		
	return max

def coletaDados(arquivo):
	'''
	Função que recebe o nome do arquivo contendo os dados do experimentos e armazena essas informações.
	'''
	tempo = []
	coss = []
	pos = []
	with open(arquivo,"r") as txt:
		 qtd = txt.readline() 
		 while (qtd != ""):
		 	coss.append(float(txt.readline().replace("\n","")))	 
		 	tt = []
		 	pp = []
		 	for i in range(0,int(qtd)):
		 		linha = txt.readline()
		 		linha = linha.replace("\n","")
		 		x = linha.split(" ")
		 		if (len(tt) == 0):
		 			tinicial = float(x[0])
		 			pinicial = float(x[1])
		 			t = 0
		 			p = 0
		 		else:  
		 			t = float(x[0]) - tinicial
		 			p = float(x[1]) - pinicial
		 		tt.append(t)
		 		pp.append(p)	 	
		 	tempo.append(tt)
		 	pos.append(pp)
		 	qtd = txt.readline()

	return [tempo,pos,coss]


def posicao(tempo,posicao):
	'''
	Função que plota a posição do bloco x tempo
	'''
	for i in range(0,len(tempo)):		
		label = labels[i]
		# Gráfico sobreposto
		plot(tempo[i],posicao[i],label,0,0,"","Sobreposição dos experimentos","Tempo(s)","Posição(cm)",0)

		#Gráfico individual
		plot(tempo[i],posicao[i],"",0,0,"","Posição(cm) " + label,"Tempo(s)","Posição(cm)",i+1)

	pyplot.show()
	return

def velocidade(tempo,posicao):
	'''
	Função que plota a velocidade do bloco x tempo
	'''
	vel_media_acum = 0
	for i in range(0,len(tempo)):
		label = labels[i]
		t = tempo[i]
		p = posicao[i]
		v = []
		t2 = []		
		for j in range(0,len(t)-1):
			ds = p[j+1] - p[j]
			dt = t[j+1] - t[j]
			if len(t2) == 0:
				t2.append(dt)
			else:
				t2.append(t2[len(t2)-1]+dt)
			v.append(ds/dt)

		# Cálculo da velocidade média de cada experimento
		vel_media = (p[len(p)-1] - p[0]) / (t[len(t)-1] - t[0])
		vel_media_acum += vel_media
		print("Velocidade média " + label + ": " + str(vel_media) + " cm/s")

		# Gráfico sobreposto
		plot(t2,v,label,0,0,"","Sobreposição dos experimentos","Tempo(s)","Velocidade(cm/s)",0)

		#Gráfico individual
		plot(t2,v,"",0,0,"","Velocidade(cm/s) " + label,"Tempo(s)","Velocidade(cm/s)",i+1)

	vel_media_acum = vel_media_acum/len(tempo)
	print("Média das velocidades médias: " + str(vel_media_acum) + " cm/s")
	pyplot.show()
	return

def aceleracao(tempo,posicao):
	'''
	Função que plota a aceleração x tempo.
	'''
	for i in range(0,len(tempo)):
		label = labels[i]
		t = tempo[i]
		p = posicao[i]
		tam = len(tempo[i])
		v = []
		dt = []
		t_acum = 0
		for j in range(0,tam-1):
			t_acum = t_acum + t[j+1]-t[j]
			dt.append(t_acum)
			v.append((p[j+1]-p[j])/(t[j+1]-t[j]))

		t_acum = 0
		dt2 = []
		ac = []
		for j in range(0,len(v)-1):
			t_acum = t_acum + dt[j+1]-dt[j]
			dt2.append(t_acum)
			ac.append((v[j+1]-v[j])/(dt[j+1]-dt[j]))

		# Gráfico sobreposto
		plot(dt2,ac,label,0,0,"","Sobreposição dos experimentos","Tempo(s)","Aceleração(cm/s²)",0)

		#Gŕafico individual
		plot(dt2,ac,"",0,0,"","Aceleração(cm/s²) " + label,"Tempo(s)","Aceleração(cm/s²)",i+1)

	pyplot.show()
	return	

def inclinacao(coss):
	'''
	Recebe os cossenos de cada experimento e devolve a inclinação da rampa
	'''
	inclinacoes = []
	for i in range(0,len(coss)):
		cos = coss[i]
		inc = (90-180*numpy.arccos(cos)/math.pi)
		print("Inclinacao ",labels[i],": ",inc,"°",sep='')
		inclinacoes.append(inc)
	return inclinacoes

def plot(v1,v2,legenda1,v3,v4,legenda2,title,xlabel,ylabel,i):
	'''
	Plota o gráfico correspondente aos dados presentes nos argumentos
	'''
	if i != -1:
		pyplot.figure(i)
	else:
		pyplot.figure()
	pyplot.plot(v1,v2,label=legenda1)
	if (v3 != 0 and v4 != 0):
		pyplot.plot(v3,v4,label=legenda2)

	if (legenda1 == ""):
		pyplot.plot(v1[-1],v2[-1],"ro")
		pyplot.annotate("(%.2f,%.2f)" %(v1[-1],v2[-1]), xy=(v1[-1], v2[-1]), xytext=(v1[-1] - maxAbs(v1)*0.2, v2[-1]))
	
	pyplot.title(title)
	pyplot.xlabel(xlabel)
	pyplot.ylabel(ylabel)
	if (legenda1 != "" or legenda2 != ""):
		pyplot.legend()
	return

def experimentos(dados):
	'''
	Chama as análises dos experimentos
	dados[0] = tempo
	dados[1] = posição
	dados[2] = cossenos
	'''
	inclinacao(dados[2])
	posicao(dados[0],dados[1])
	velocidade(dados[0],dados[1])
	aceleracao(dados[0],dados[1])

def novaAcel(angulo,coef):
	'''
	Calcula a aceleração do bloco levando em conta o atrito.
	'''
	angulo= math.pi*angulo/180
	fg = massa * g
	fi = fg * math.sin(angulo)
	fn = fg * math.cos(angulo)
	ff = coef * fn
	a = abs(fi-ff)/massa
	return a

def atualizaPasso(t,dt,acc):
	a = acc
	t = t + dt
	s = (a * t * t)/2
	v = t * a 
	return[t,s,v,a]


def modelo(angulo,coef=0.31):
	'''
	Função que modela o movimento do bloco de aço na rampa
	'''
	t = 0
	tf = 0.35
	dt = 0.01
	v = 0
	s = 0
	if (coef != 0):
		acc = novaAcel(angulo,coef)
	else:
		acc = g * math.sin(math.pi*angulo/180)
	tt = []
	vel = []
	pos = []
	acel = []
	while (t <= tf):		
		vel.append(v)
		tt.append(t)
		pos.append(s)
		acel.append(acc)
		resp = atualizaPasso(t,dt,acc)
		t = resp[0]
		s = resp[1]
		v = resp[2]
		acc = resp[3]
		
	# Calculando velocidade média e média das velocidades
	vel_media = (vel[len(vel)-1] - vel[0]) / abs(tt[len(tt)-1]-tt[0])
	print("Velocidade média (modelo): ",vel_media,"cm/s")

	# Posição em cm
	plot(tt,pos,"",0,0,"","Posição x Tempo (modelo)","Tempo(s)","Posição(cm)",-1)

	# Velocidade em cm/s
	plot(tt,vel,"",0,0,"","Velocidade x Tempo (modelo)","Tempo(s)","Velocidade(cm/s)",-1)

	# Aceleração angular (cm/s²)
	plot(tt,acel,"",0,0,"","Aceleração x Tempo (modelo)","Tempo(s)","Aceleração(cm/s²)",-1)

	return [tt,pos,vel,acel]

def compara(atrito,dados,exp):
	if(atrito):
		paramModelo = modelo(inclinacao(dados[2])[exp],CoefsAtrito[exp])
	else:
		paramModelo = modelo(inclinacao(dados[2])[exp],0)
	# Refazendo os cálculos para o experimento
	tempo = dados[0][exp]
	posicao = dados[1][exp]
	velcm = []
	acc = []
	tvel = []
	for i in range(0,len(tempo)-1):
		v = (abs(posicao[i+1]-posicao[i]))/(tempo[i+1]-tempo[i])
		if (len(tvel) == 0):
			ultimo = 0
		else:
			ultimo = tvel[len(tvel)-1]
		tvel.append(ultimo+tempo[i+1]-tempo[i])
		velcm.append(v)
	tacc = []
	for i in range(0,len(velcm)-1):
		a = (velcm[i+1]-velcm[i])/(tvel[i+1]-tvel[i])
		if (len(tacc) == 0):
			ultimo = 0
		else:
			ultimo = tacc[len(tacc)-1]
		tacc.append(ultimo+tvel[i+1]-tvel[i])
		acc.append(a)
	
	# Plotando os gŕaficos
	pyplot.close('all')

	plot(tempo,posicao,"Experimento",paramModelo[0],paramModelo[1],"Modelo","Modelo x experimento - Posição(cm)","Tempo(s)","Posição(cm)",-1)

	plot(tvel,velcm,"Experimento",paramModelo[0],paramModelo[2],"Modelo","Modelo x experimento - Velocidade(cm/s)","Tempo(s)","Velocidade(cm/s)",-1)

	plot(tacc,acc,"Experimento",paramModelo[0],paramModelo[3],"Modelo","Modelo x experimento - Aceleração(cm/s²)","Tempo(s)","Aceleração(cm/s²)",-1)

	pyplot.show()
	return


## Testes

def criaDadosTeste(idTeste):
	'''
		Cria os dados para casos de teste;
	'''
	tempos = [0.0]
	posicao = [0]
	if idTeste == 1:
		Inc = 0
		cosseno = math.cos(math.pi/2 -Inc) # 0° de inclinação
		for _ in range(30):
			tempos.append(tempos[-1]+0.01)
			posicao.append(0)
	elif idTeste == 2:
		Inc = 12 # inclinação
		pi = math.pi
		cosseno = math.cos(pi/2 - pi*Inc/180) 
		acc = g * math.sin(pi*Inc/180)
		vel = 0
		for _ in range(30):
			tempos.append(tempos[-1]+0.01)			
			posicao.append(acc*tempos[-1]*tempos[-1]/2)
	elif idTeste == 3:
		Inc = 12
		pi = math.pi
		cosseno = math.cos(pi/2 - pi*Inc/180) 
		acc = abs(massa*g*(math.sin(pi*Inc/180)-0.31*math.cos(pi*Inc/180)))/massa
		vel = 0
		for _ in range(30):
			tempos.append(tempos[-1]+0.01)
			posicao.append(acc*tempos[-1]*tempos[-1]/2)			
	return ([tempos],[posicao],[cosseno])

def testes():
	'''
		Testa os métodos de análise de dados de experi-
	mentos, de criação de gráficos, de modelagem e de comparação
	'''
	print("Teste 1 - Experimento - Objeto Imóvel (plano horizontal)")

	dados1 = criaDadosTeste(1)
	experimentos(dados1)

	print("Teste 2- Plano inclinado sem atrito")

	dados2 = criaDadosTeste(2)
	experimentos(dados2)
	compara(False,dados2,0)

	print("Teste 3 - Plano inclinado com atrito")

	dados3 = criaDadosTeste(3)
	experimentos(dados3)
	compara(True,dados3,0)


def main():
	if len(sys.argv) != 2:
		print(" Modo de execução:\n\tpython %s <nomeArq>\n Onde <nomeArq> é o nome de um arquivo.txt neste mesmo diretório que contem os dados dos experimentos da rampa a serem analisados." %sys.argv[0])
		return
	nomeArq = sys.argv[1]
	dados = coletaDados(nomeArq)
	pyplot.show()
	while (True):
		modo = int(input("O que deseja ver?\n1.Experimentos\n2.Modelo\n3.Comparação\n4.Testes\n5.Sair\n"))
		if modo == 1:
			experimentos(dados)		
		elif modo == 2:		
			atrito = input("Deseja modelar com atrito? ")
			if atrito == "S" or atrito == "Sim" or atrito == "sim" or atrito == "s":
				atrito = True
				modelo(12,CoefsAtrito[0])
			else:
				atrito = False
				modelo(12,0)			
		elif modo == 3:
			atrito = input("Deseja modelar com atrito? ")
			if atrito == "S" or atrito == "Sim" or atrito == "sim" or atrito == "s":
				atrito = True
			else:
				atrito = False
			exp = int(input("Qual experimento deseja comparar com o modelo(1-5): ")) - 1
			compara(atrito,dados,exp)
		elif modo == 4:
			testes()
		else:
			quit()
		pyplot.show()
main()
